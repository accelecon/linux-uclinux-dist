/*
 *  Accelerated Concepts NetBridge board support
 *
 *  Copyright (C) 2011-2012 Gabor Juhos <juhosg@openwrt.org>
 *  Copyright (C) 2012 Greg Ungerer <greg.ungerer@accelecon.com>
 *
 *  This program is free software; you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License version 2 as published
 *  by the Free Software Foundation.
 */

#include <linux/gpio.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/partitions.h>
#include <linux/platform_device.h>
#include <linux/i2c.h>
#include <linux/i2c-gpio.h>
#include <linux/i2c/pca953x.h>

#include <asm/mach-ath79/ath79.h>
#include <asm/mach-ath79/ar71xx_regs.h>

#include "common.h"
#include "dev-eth.h"
#include "dev-gpio-buttons.h"
#include "dev-leds-gpio.h"
#include "dev-m25p80.h"
#include "dev-usb.h"
#include "dev-wmac.h"
#include "machtypes.h"

#define NETBRIDGE_UB_MAC0_OFFSET	0x0006
#define NETBRIDGE_UB_MAC1_OFFSET	0x0000
#define NETBRIDGE_UB_CALDATA_OFFSET	0x1000

/*
 * Define the CPU GPIO pins.
 */
#define	NETBRIDGE_GPIO_RSSI_5		1
#define	NETBRIDGE_GPIO_RSSI_4		13
#define	NETBRIDGE_GPIO_RSSI_3		14
#define	NETBRIDGE_GPIO_RSSI_2		15
#define	NETBRIDGE_GPIO_RSSI_1		16
#define	NETBRIDGE_GPIO_WIFI		17
#define	NETBRIDGE_GPIO_LAN_LED_1	19
#define	NETBRIDGE_GPIO_LAN_LED_3	21
#define	NETBRIDGE_GPIO_LAN_LED_2	22
#define	NETBRIDGE_GPIO_LAN_LED_0	23

#define	NETBRIDGE_GPIO_WDT_WDI		26
#define	NETBRIDGE_GPIO_WDT_VDD_N	27

/*
 * Define the I2C I/O expander GPIO pins.
 */
#define	NETBRIDGE_GPIO_IO_BASE		32
#define	NETBRIDGE_GPIO_USB_OC2		32
#define	NETBRIDGE_GPIO_USB_ENABLE2	33
#define	NETBRIDGE_GPIO_SW_LEFT		35
#define	NETBRIDGE_GPIO_SW_CENTER	36
#define	NETBRIDGE_GPIO_SW_UP		37
#define	NETBRIDGE_GPIO_SW_RIGHT		38
#define	NETBRIDGE_GPIO_SW_DOWN		39
#define	NETBRIDGE_GPIO_USB_ENABLE1	40
#define	NETBRIDGE_GPIO_USB_OC1		41
#define	NETBRIDGE_GPIO_USB_HUB_RESET	42
#define	NETBRIDGE_GPIO_USB_OC3		43
#define	NETBRIDGE_GPIO_USB_ENABLE3	44

#define	KEY_POLL_INTERVAL		50	/* ms */
#define	KEY_DEBOUNCE_INTERVAL		(2 * KEY_POLL_INTERVAL)

/*
 * Define our flash layout.
 */
#if 1
/*
 *	This is our preferred layout, will need beter u-boot support before
 *	we can use it though.
 */
static struct mtd_partition netbridge_partitions[] = {
	{
		.name		= "u-boot",
		.offset		= 0,
		.size		= 0x040000,
		.mask_flags	= MTD_WRITEABLE,
	}, {
		.name		= "u-boot-env",
		.offset		= 0x040000,
		.size		= 0x010000,
	}, {
		.name		= "config",
		.offset		= 0x050000,
		.size		= 0x100000,
	}, {
		.name		= "rootfs",
		.offset		= 0x150000,
		.size		= 0xcb0000,
	}, {
		.name		= "kernel",
		.offset		= 0xe00000,
		.size		= 0x1f0000,
	}, {
		.name		= "image",
		.offset		= 0x150000,
		.size		= 0xea0000,
	}, {
		.name		= "art",
		.offset		= 0xff0000,
		.size		= 0x010000,
		.mask_flags	= MTD_WRITEABLE,
	}
};
#endif
#if 0
/*
 *	This layout matches the default that came on the board from Alfa.
 */
static struct mtd_partition netbridge_partitions[] = {
	{
		.name		= "u-boot",
		.offset		= 0,
		.size		= 0x040000,
	}, {
		.name		= "u-boot-env",
		.offset		= 0x040000,
		.size		= 0x010000,
	}, {
		.name		= "rootfs",
		.offset		= 0x050000,
		.size		= 0xeb0000,
	}, {
		.name		= "kernel",
		.offset		= 0xf00000,
		.size		= 0x0e0000,
	}, {
		.name		= "nvram",
		.offset		= 0xfe0000,
		.size		= 0x010000,
	}, {
		.name		= "art",
		.offset		= 0xff0000,
		.size		= 0x010000,
		.mask_flags	= MTD_WRITEABLE,
	}
};
#endif

static struct flash_platform_data netbridge_flash_data = {
	.parts		= netbridge_partitions,
	.nr_parts	= ARRAY_SIZE(netbridge_partitions),
};

static struct i2c_gpio_platform_data netbridge_i2c_gpio_data = {
	.sda_pin        = 20,
	.scl_pin        = 18,
};

static struct platform_device netbridge_i2c_gpio_device = {
	.name		= "i2c-gpio",
	.id		= 0,
	.dev = {
		.platform_data	= &netbridge_i2c_gpio_data,
	}
};

static struct pca953x_platform_data netbridge_pca953x_data = {
	.gpio_base	= NETBRIDGE_GPIO_IO_BASE,
};

static struct i2c_board_info netbridge_i2c_board_info[] __initdata = {
	{
		I2C_BOARD_INFO("pca9555", 0x20),
		.platform_data  = &netbridge_pca953x_data,
	},
	{
		I2C_BOARD_INFO("pcf2116", 0x3a),
	},
};

#ifndef CONFIG_LEDMAN
/*
 * Define the board LEDs (only if ledman is not being used).
 */
static struct gpio_led netbridge_gpio_leds[] __initdata = {
	{
		.name		= "rssi1",
		.gpio		= NETBRIDGE_GPIO_RSSI_1,
		.active_low	= 0,
	},
	{
		.name		= "rssi2",
		.gpio		= NETBRIDGE_GPIO_RSSI_2,
		.active_low	= 0,
	},
	{
		.name		= "rssi3",
		.gpio		= NETBRIDGE_GPIO_RSSI_3,
		.active_low	= 0,
	},
	{
		.name		= "rssi4",
		.gpio		= NETBRIDGE_GPIO_RSSI_4,
		.active_low	= 0,
	},
	{
		.name		= "rssi5",
		.gpio		= NETBRIDGE_GPIO_RSSI_5,
		.active_low	= 0,
	},
	{
		.name		= "wifi",
		.gpio		= NETBRIDGE_GPIO_WIFI,
		.active_low	= 0,
	},
	{
		.name		= "lanled0",
		.gpio		= NETBRIDGE_GPIO_LAN_LED_0,
		.active_low	= 0,
	},
	{
		.name		= "lanled1",
		.gpio		= NETBRIDGE_GPIO_LAN_LED_1,
		.active_low	= 0,
	},
	{
		.name		= "lanled2",
		.gpio		= NETBRIDGE_GPIO_LAN_LED_2,
		.active_low	= 0,
	},
	{
		.name		= "lanled3",
		.gpio		= NETBRIDGE_GPIO_LAN_LED_3,
		.active_low	= 0,
	},
};
#endif /* CONFIG_LEDMAN */

/*
 * Define the JOG switch buttons for the gpio-keys input subsystem.
 */
static struct gpio_keys_button netbridge_gpio_keys[] __initdata = {
	{
		.desc		= "Left button",
		.type		= EV_KEY,
		.code		= KEY_LEFT,
		.debounce_interval = KEY_DEBOUNCE_INTERVAL,
		.gpio		= NETBRIDGE_GPIO_SW_LEFT,
		.active_low	= 1,
	},
	{
		.desc		= "Center button",
		.type		= EV_KEY,
		.code		= KEY_ENTER,
		.debounce_interval = KEY_DEBOUNCE_INTERVAL,
		.gpio		= NETBRIDGE_GPIO_SW_CENTER,
		.active_low	= 1,
	},
	{
		.desc		= "Up button",
		.type		= EV_KEY,
		.code		= KEY_UP,
		.debounce_interval = KEY_DEBOUNCE_INTERVAL,
		.gpio		= NETBRIDGE_GPIO_SW_UP,
		.active_low	= 1,
	},
	{
		.desc		= "Right button",
		.type		= EV_KEY,
		.code		= KEY_RIGHT,
		.debounce_interval = KEY_DEBOUNCE_INTERVAL,
		.gpio		= NETBRIDGE_GPIO_SW_RIGHT,
		.active_low	= 1,
	},
	{
		.desc		= "Down button",
		.type		= EV_KEY,
		.code		= KEY_DOWN,
		.debounce_interval = KEY_DEBOUNCE_INTERVAL,
		.gpio		= NETBRIDGE_GPIO_SW_DOWN,
		.active_low	= 1,
	}
};

static void __init netbridge_soc_gpio_setup(void)
{
	u32 t;

	ath79_gpio_function_disable(AR933X_GPIO_FUNC_ETH_SWITCH_LED0_EN |
				     AR933X_GPIO_FUNC_ETH_SWITCH_LED1_EN |
				     AR933X_GPIO_FUNC_ETH_SWITCH_LED2_EN |
				     AR933X_GPIO_FUNC_ETH_SWITCH_LED3_EN |
				     AR933X_GPIO_FUNC_ETH_SWITCH_LED4_EN);

	t = ath79_reset_rr(AR933X_RESET_REG_BOOTSTRAP);
	t |= AR933X_BOOTSTRAP_MDIO_GPIO_EN;
	ath79_reset_wr(AR933X_RESET_REG_BOOTSTRAP, t);
}

static int __init netbridge_gpio_setup(void)
{
#if 0
	/* Setup GPIOs that run the external power watchdog */
	gpio_request(NETBRIDGE_GPIO_WDT_WDI, "WDI");
	gpio_direction_output(NETBRIDGE_GPIO_WDT_WDI, 1);
	gpio_request(NETBRIDGE_GPIO_WDT_VDD_N, "WDT-VDD-N");
	gpio_direction_output(NETBRIDGE_GPIO_WDT_VDD_N, 1);
	gpio_set_value_cansleep(NETBRIDGE_GPIO_WDT_VDD_N, 1);
#endif

	gpio_request(0, "IO_INT");
	gpio_direction_input(0);
	return 0;
}

static int __init netbridge_i2c_gpio_setup(void)
{
	/* Turn on the USB power chips */
	gpio_request(NETBRIDGE_GPIO_USB_OC1, "USB1 over-current");
	gpio_direction_input(NETBRIDGE_GPIO_USB_OC1);
	gpio_request(NETBRIDGE_GPIO_USB_ENABLE1, "USB1 power");
	gpio_direction_output(NETBRIDGE_GPIO_USB_ENABLE1, 1);
	gpio_set_value_cansleep(NETBRIDGE_GPIO_USB_ENABLE1, 0);

	gpio_request(NETBRIDGE_GPIO_USB_OC2, "USB2 over-current");
	gpio_direction_input(NETBRIDGE_GPIO_USB_OC2);
	gpio_request(NETBRIDGE_GPIO_USB_ENABLE2, "USB2 power");
	gpio_direction_output(NETBRIDGE_GPIO_USB_ENABLE2, 1);
	gpio_set_value_cansleep(NETBRIDGE_GPIO_USB_ENABLE2, 0);

	gpio_request(NETBRIDGE_GPIO_USB_OC3, "USB3 over-current");
	gpio_direction_input(NETBRIDGE_GPIO_USB_OC3);
	gpio_request(NETBRIDGE_GPIO_USB_ENABLE3, "USB3 power");
	gpio_direction_output(NETBRIDGE_GPIO_USB_ENABLE3, 1);
	gpio_set_value_cansleep(NETBRIDGE_GPIO_USB_ENABLE3, 0);

	gpio_request(NETBRIDGE_GPIO_USB_HUB_RESET, "USB Hub Reset");
	gpio_direction_output(NETBRIDGE_GPIO_USB_HUB_RESET, 1);
	gpio_set_value_cansleep(NETBRIDGE_GPIO_USB_HUB_RESET, 1);

	/* Setup the JOG switch keys */
	ath79_register_gpio_keys_polled(-1, KEY_POLL_INTERVAL,
		ARRAY_SIZE(netbridge_gpio_keys), netbridge_gpio_keys);

	return 0;
}
late_initcall(netbridge_i2c_gpio_setup);

static void __init netbridge_setup(void)
{
	u8 *art = (u8 *) KSEG1ADDR(0x1fff0000);

	netbridge_gpio_setup();
	netbridge_soc_gpio_setup();

	i2c_register_board_info(0, netbridge_i2c_board_info,
		ARRAY_SIZE(netbridge_i2c_board_info));
	platform_device_register(&netbridge_i2c_gpio_device);

#ifndef CONFIG_LEDMAN
	ath79_register_leds_gpio(-1, ARRAY_SIZE(netbridge_gpio_leds),
		netbridge_gpio_leds);
#endif

	ath79_register_m25p80(&netbridge_flash_data);

	ath79_init_mac(ath79_eth1_data.mac_addr,
			art + NETBRIDGE_UB_MAC0_OFFSET, 0);
	ath79_init_mac(ath79_eth0_data.mac_addr,
			art + NETBRIDGE_UB_MAC1_OFFSET, 0);

	ath79_register_mdio(0, 0x0);

	ath79_register_eth(1);
	ath79_register_eth(0);

	ath79_register_wmac(art + NETBRIDGE_UB_CALDATA_OFFSET, NULL);
	ath79_register_usb();
}

MIPS_MACHINE(ATH79_MACH_NETBRIDGE, "NETBRIDGE",
	"Accelerated Concepts NetBridge", netbridge_setup);
